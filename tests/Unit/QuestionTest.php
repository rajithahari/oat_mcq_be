<?php

namespace Tests\Unit;
use Tests\TestCase;

class QuestionTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_invalid_langauage_argument()
    {
        $response = $this->get(route('index'), )->assertStatus(200);
    }

    public function test_valid_langauage_argument()
    {
        $response = $this->get(route('index'))->assertStatus(200);
    }

    public function test_valid_data_to_store()
    {
        $data =  [
            "text" => "what is an", 
            "createdAt" => "2020-06-02 00:00:00", 
            "choices" => [
                [
                    "text" => "abc" 
                ], 
                [
                    "text" => "fgh" 
                ], 
                [
                    "text" => "cde" 
                ] 
            ] 
        ];
        $response = $this->post(route('save'), $data)->assertStatus(200);
    }

    public function test_invalid_data_to_store()
    {
        $data =  [
            "text" => "what is an", 
            "createdAt" => "2020-06-02 00:00:00", 
            "choices dfsf" => [
                    [
                        "text" => "abc" 
                    ], 
                    [
                        "text" => "fgh" 
                    ], 
                    [
                        "text" => "cde" 
                    ] 
                ] 
            ];
        $response = $this->post(route('save'), $data)->assertStatus(200);
    }

    public function test_invalid_choices_data_to_store()
    {
        $data =  [
            "text" => "what is an", 
            "createdAt" => "2020-06-02 00:00:00", 
            "choices" => [
                  [
                     "text" => "abc" 
                  ],
                ] 
            ];
        
        $response = $this->post(route('save'), $data)->assertStatus(200);
    }
}
