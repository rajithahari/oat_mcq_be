##### OAT MCQ ##########

1. Install all the dependencies using composer
````````````````````````````````````````
    composer install

2. Package Installed: 
`````````````````````
    stichoza/google-translate-php

    nunomaduro/phpinsights --dev


3. Set up Environment file
````````````````````````

    Can configure data source from .env by copying the content from .env.example file


4. Start the local development server
````````````````````````````````````

    php artisan serve


5. End Points
```````````````

    End point URL - Get All Questions : http://127.0.0.1:8000/api/questions?lang=en

    End point URL - Post a Question : http://127.0.0.1:8000/api/questions 
    Request Payload: 

    {
        "text": "what is an",
        "createdAt": "2021-09-03 00:00:00",
        "choices": [
            {
                "text": "abc"
            },
            {
                "text": "fgh"
            },
            {
                "text": "cde"
            }
        ]
    }

6. Unit Test

    php artisan test

7. Cache
    
    Default we are storing in the cacche file driver. As per requirement, we can store in redis, memcached, db

##### OAT MCQ ##########