<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Validator;

class QuestionController extends Controller
{
    protected $dataSource;
    public function __construct()
    {
        $this->dataSource = app()->make('DatasourceService');
    }

    /** Display a listing of the questions with the source */
    public function index(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'lang' => 'required|max:5',
        ]);
        if ($validate->fails()) { // Validation fails, return empty data with status
            $errors = $validate->errors();
            $data = [
                'status' => false,
                'data' => [],
                'message' => $errors->first(),
            ];
            return response()->json($data, 200);
        }
        try { // Validation success, return data with success status
            $questions = $this->listQuestions();
            $data = [
                'status' => true,
                'data' => $this->translate($questions, $request),
                'message' => 'Success',
            ];
            return response()->json($data, 200);
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    // Method to translate the content
    public function translate(array $data, Request $request)
    {
        $lang = $request->input('lang');
        $translate = new GoogleTranslate();
        $translate->setSource('en');
        $translate->setTarget($lang);
        $translatedArray = [];
        foreach ($data as $item) {
            $text = '';
            if (Cache::has($lang.':'.$translate->translate($item['text']))) {
                $text = Cache::get($lang.':'.$translate->translate($item['text']));
            } else {
                $text = $translate->translate($item['text']);
                Cache::put($lang.':'.$translate->translate($item['text']), $text);
            }
            $item = [
                'text' => $text,
                'choices' => $this->getChoices($item['choices'], $translate, $request),
            ];
            array_push($translatedArray, $item);
        }
        return $translatedArray;
    }

    // Choices translating here
    public function getChoices($choices, $translate, Request $request)
    {
        $newArray = [];
        $lang = $request->input('lang');
        foreach ($choices as $choice) {
            $text = '';
            if (Cache::has($lang.':'.$translate->translate($choice['text']))) {
                $text = Cache::get($lang.':'.$translate->translate($choice['text']));
            } else {
                $text = $translate->translate($choice['text']);
                Cache::put($lang.':'.$translate->translate($choice['text']), $text);
            }
            array_push($newArray, $translate->translate($text));
        }
        return $newArray;
    }

    /**
     * Save method to create new question */
    public function save(Request $request)
    {
        // Validating the required parameter language and its length
        $validate = Validator::make($request->all(), [
            'text' => 'required|max:255',
            'createdAt' => 'required|date',
            'choices' => ['required','array','max:3','min:3'],
        ]);
        if ($validate->fails()) { // Validation fails, return empty data with status
            $errors = $validate->errors();
            $data = [
                'status' => false,
                'data' => [],
                'message' => $errors->first(),
            ];
            return response()->json($data, 200);
        }
        try {
            $data = [
                'status' => true,
                'data' => $this->createQuestion(),
                'message' => 'Success',
            ];
            return response()->json($data, 200);
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    // function to get list of questions based on datasource
    public function listQuestions()
    {
        return $this->dataSource->getSource(); // Get the source from Service Provider
    }

    // function to create questions based on datasource
    public function createQuestion()
    {
        $payLoad = json_decode(request()->getContent(), true); // Payload conversion
        return $this->dataSource->saveData($payLoad);
    }
}
