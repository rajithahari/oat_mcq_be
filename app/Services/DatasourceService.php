<?php

namespace App\Services;

class DatasourceService
{
    protected $csvService;
    protected $jsonService;
    protected $source;

    public function __construct()
    {
        $this->source = env('SOURCE');
        $this->csvService = app()->make('CsvService');
        $this->jsonService = app()->make('JsonService');
    }

    public function getSource()
    {
        $data = [];
        switch ($this->source) {
            case 'json':
                $data = $this->jsonService->getJsonData();
                break;
            case 'csv':
                $data = $this->csvService->getCsvData();
                break;
        }
        return $data;
    }

    public function saveData($payLoad = [])
    {
        switch ($this->source) {
            case 'json':
                $data = $this->jsonService->saveToJson($payLoad); // Function call to save the the data as Json
                break;
            case 'csv':
                $data = $this->csvService->saveToCSV($payLoad); // Function call to save the the data as Csv
                break;
            case 'mysql': // To be implimented in future
                break;
        }
        return $data;
    }
}
