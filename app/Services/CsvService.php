<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class CsvService
{
    // Method to get CSV data from the file
    public function getCsvData()
    {
        $filename = Storage::disk('local')->path(env('SOURCE_CSV'));
        return $this->csvToArray($filename);
    }

    // convert csv to array
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (! file_exists($filename) || ! is_readable($filename)) { // If file exists or not
            return false;
        }
        $data = [];
        // If file exists, reading the csv and save as array
        $handle = fopen($filename, 'r');
        if ($handle !== false) {
            $data = $this->loopThroughData($handle, $delimiter); // Method to get through the csv data
            fclose($handle);
        }
        return $data;
    }

    // Get csv contents
    public function loopThroughData($handle, $delimiter)
    {
        $header = null;
        $data = $choiceArray = $questionArray = [];
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
            if (! $header) {
                $header = ['text', 'createdAt', 'choices'];
            } else {
                $choiceArray = [];
                $choices = array_slice($row, count($header) - 1);
                $choiceCount = count($choices);
                for ($i = 0; $i < $choiceCount; $i++) {
                    $dataItem = [
                        'text' => $choices[$i],
                    ];
                    array_push($choiceArray, $dataItem);
                }
                $questionArray = array_slice($row, 0, count($header) - 1);
                array_push($questionArray, $choiceArray);
                array_push($data, array_combine($header, $questionArray));
            }
        }
        return $data;
    }

    // Method to save CSV
    public function saveToCSV($payLoad)
    {
        $newArray = [];
        $filename = Storage::disk('local')->path(env('SOURCE_CSV')); // Getting the file from the path
        $file_open = fopen($filename, 'a');
        foreach ($payLoad['choices'] as $choice) {
            array_push($newArray, $choice['text']);
        }
        $questionArray = array_slice($payLoad, 0, 2);
        $finalArray = array_merge($questionArray, $newArray);
        fputcsv($file_open, $finalArray);
        return $payLoad;
    }
}
