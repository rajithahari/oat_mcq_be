<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class JsonService
{
    // Method to get Json dat from the file
    public function getJsonData()
    {
        $filename = Storage::disk('local')->path(env('SOURCE_JSON'));
        $str = file_get_contents($filename);
        return json_decode($str, true);
    }

    // Method saving Json
    public function saveToJson($payLoad)
    {
        $filename = Storage::disk('local')->path(env('SOURCE_JSON')); // Getting the file from the path
        $inp = file_get_contents($filename);
        $tempArray = json_decode($inp);
        array_push($tempArray, $payLoad);
        $jsonData = json_encode($tempArray);
        file_put_contents($filename, $jsonData);
        return $payLoad;
    }
}
