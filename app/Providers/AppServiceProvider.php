<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('DatasourceService', \App\Services\DatasourceService::class);
        $this->app->bind('JsonService', \App\Services\JsonService::class);
        $this->app->bind('CsvService', \App\Services\CsvService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
